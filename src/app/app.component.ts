import { Component, OnInit } from '@angular/core';
import { Persona } from './persona.model';
import { EstePrsonasService } from './Personas.service';
import * as firebase from 'firebase';
import { LoginService } from './login.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  titulo = 'Listado-Personas';
  // personas: Persona[] = [];

  // personas: Persona[] = [ new Persona('juan','0perez'), new Persona('Laura', 'Juarez')];
  constructor(private personaService: EstePrsonasService,
              private loginSer: LoginService) {}

// tslint:disable-next-line: use-lifecycle-interface
    ngOnInit(): void {
      firebase.initializeApp({
        apiKey: 'AIzaSyBmdffwoqCx4KKWORojbPClUNfooTsGlwo',
        authDomain: 'pruebas-firebase-b9b1d.firebaseapp.com',
      });
      // this.personas = this.personaService.personas;
    }

  // onPersonaAgreagada(persona: Persona) {
    // this.personas.push(persona);
    // this.personaService.agregarPersona(persona);
  // }
  isAutenticado() {
    return this.loginSer.isAutenticado();
  }

  salir() {
    this.loginSer.logout();
  }
}
