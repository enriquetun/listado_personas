import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Persona } from './persona.model';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  url = 'https://pruebas-firebase-b9b1d.firebaseio.com/datos.json';
  constructor(private httpClient: HttpClient,
              private loginSer: LoginService) { }

  cargarPersonas() {
    const tok = this.loginSer.getIdToken();
    return this.httpClient.get(this.url + '?auth=' + tok);
  }

  // Guardar personas
  guardarPersonas(personas: Persona[]) {
// tslint:disable-next-line: prefer-const
    // se agrego el metodo post para poder crear todo una base de datos
    const tok = this.loginSer.getIdToken();
    this.httpClient.put(this.url + '?auth=' + tok, personas)
    .subscribe(
      response => {
        console.log('resultado gurrdado Personas: ' + response);
      }, error => {
        console.log('Error al guardar perosnas:' + error);
      }
    );
  }

  modificarPersona(index: number, persona: Persona) {
// tslint:disable-next-line: ban-types
    const tok = this.loginSer.getIdToken();
    let url: string;
    url = 'https://pruebas-firebase-b9b1d.firebaseio.com/datos/' + index + '.json?auth=' + tok;
    this.httpClient.put(url, persona)
    .subscribe(
      response => { console.log('resultado modificar Persona:' + response);
    },
      error => {console.log(error);
      }
    );
  }
  eliminarPersona(index: number) {
    const tok = this.loginSer.getIdToken();
    let url: string;
    url = 'https://pruebas-firebase-b9b1d.firebaseio.com/datos/' + index + '.json?auth=' + tok;
    this.httpClient.delete(url)
    .subscribe(
      response => console.log('resultado al eliminar persona:', response),
      error => console.log('Error en eliminar persona' + error)
    );
  }
}
