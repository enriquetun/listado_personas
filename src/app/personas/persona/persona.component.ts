import { Component, OnInit, Input } from '@angular/core';
import { Persona } from '../../persona.model';
import { EstePrsonasService } from '../../Personas.service';


@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.css']
})
export class PersonaComponent implements OnInit {
  @Input() persona: Persona;
  @Input() indice: number;

  constructor(private personaSerrvice: EstePrsonasService) { }

  ngOnInit() {
  }
  emitirSaludo() {
    this.personaSerrvice.saludar.emit(this.indice);
  }

}
