import { Component, OnInit } from '@angular/core';
import { Persona } from '../persona.model';
import { EstePrsonasService } from '../Personas.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-personas',
  templateUrl: './personas.component.html',
  styleUrls: ['./personas.component.css']
})
export class PersonasComponent implements OnInit {

  personas: Persona[] = [];

  // personas: Persona[] = [ new Persona('juan','0perez'), new Persona('Laura', 'Juarez')];
  constructor(private personaService: EstePrsonasService, private router: Router) {}

  ngOnInit(): void {
    // this.personas = this.personaService.personas;
    this.personaService.obtenerPersonas()
    .subscribe(
      (personasData: Persona[]) => {
        this.personas = personasData;
        this.personaService.setPersonas(personasData);
      }
    );
  }

  onPersonaAgreagada(persona: Persona) {
    // this.personas.push(persona);
    this.personaService.agregarPersona(persona);
  }

  agregar() {
    this.router.navigate(['personas/agregar']);
  }
}
