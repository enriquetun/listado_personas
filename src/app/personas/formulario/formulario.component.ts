import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Persona } from '../../persona.model';
import { EstePrsonasService } from '../../Personas.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {
  @Output() personaCreada = new EventEmitter<Persona>();
   nombreInput: string;
   apellidoInput: string;
  // refencia local de la plantilla
  // @ViewChild('nombreInput') nombresInput: ElementRef;
  // @ViewChild('apellidoInput') apellidoInput: ElementRef;
  index: number;
  modoEdicion: number;

  constructor(private personasService: EstePrsonasService, private router: Router, private route: ActivatedRoute) {
    this.personasService.saludar.subscribe((indice: number) => alert('El indice es: ' + indice));
   }

  ngOnInit() {
// tslint:disable-next-line: no-string-literal
    this.index = this.route.snapshot.params['id'];
// tslint:disable-next-line: no-string-literal
    this.modoEdicion =  +this.route.snapshot.queryParams['modoEdicion'];

    if (this.modoEdicion != null && this.modoEdicion === 1) {
// tslint:disable-next-line: prefer-const
      let persona: Persona = this.personasService.encontrarPersona(this.index);
      this.nombreInput = persona.nombre;
      this.apellidoInput = persona.apellido;
    }
  }

  onGuardarPersona() {
  // onGuardarPersona(nombre: HTMLInputElement, apellido: HTMLInputElement) {
  // onAgregarPersona() {
    const persona1 = new Persona(this.nombreInput, this.apellidoInput);
    // const persona1 = new Persona(nombre.value, apellido.value);
    // this.logginService.enviarMensajeAConsola('Esta persona es: ' + persona1.nombre + ' apellido:' + persona1.apellido);
    // const persona1 = new Persona(this.nombresInput.nativeElement.value, this.apellidoInput.nativeElement.value);
    // this.personaCreada.emit(persona1);
    if (this.modoEdicion != null && this.modoEdicion === 1) {
      this.personasService.modificarPersona(this.index, persona1);
    } else {
      this.personasService.agregarPersona(persona1);
    }
    this.router.navigate(['personas']);
  }

  eliminarPersona() {
    if (this.index != null) {
      this.personasService.eliminarPersona(this.index);
    }
    this.router.navigate(['personas']);
  }
}
