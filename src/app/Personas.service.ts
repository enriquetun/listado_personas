import { Persona } from './persona.model';
import { LoggingService } from './LogginService.service';
import { Injectable, EventEmitter } from '@angular/core';
import { DataService } from './data.service';

@Injectable()
export class EstePrsonasService {
    personas: Persona[] = [ new Persona('juan', 'perez'), new Persona('Laura', 'Juarez')];

    saludar = new EventEmitter<number>();

    constructor(private logginService: LoggingService,
                private dataServicio: DataService) {}

    obtenerPersonas() {
        return this.dataServicio.cargarPersonas();
    }
    agregarPersona(persona: Persona) {
        this.logginService.enviarMensajeAConsola('Desde el service guardo: ' + persona.nombre);
        if (this.personas == null) {
            this.personas = []
        }
        this.personas.push(persona);
        this.dataServicio.guardarPersonas(this.personas);
    }
    setPersonas(personas: Persona[]) {
        this.personas = personas;
    }
    encontrarPersona(index: number) {
// tslint:disable-next-line: prefer-const
        let persona: Persona =  this.personas[index];
        return persona;
    }
    modificarPersona(index: number, persona: Persona){
// tslint:disable-next-line: prefer-const
        let persona1 = this.personas[index];
        persona1.nombre = persona.nombre;
        persona1.apellido = persona.apellido;
        this.dataServicio.modificarPersona(index, persona);
    }

    eliminarPersona(index: number) {
        this.personas.splice(index, 1);
        this.dataServicio.eliminarPersona(index);
        // se vuelve a gutardar el arreglo
        this.modificarPersonas();
    }
    modificarPersonas() {
        if ( this.personas != null ) {
            this.dataServicio.guardarPersonas(this.personas);
        }
    }
}
