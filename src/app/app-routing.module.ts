import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import { PersonasComponent } from './personas/personas.component';
import { FormularioComponent } from './personas/formulario/formulario.component';
import { ErrorComponent } from './error/error.component';
import { LoginComponent } from './login/login.component';
import { LoginserviceGuard } from './loginservice.guard';


const routes: Routes = [
  {path: '', component: PersonasComponent, canActivate: [LoginserviceGuard]},
  {path: 'personas', component: PersonasComponent, canActivate: [LoginserviceGuard] , children: [
    {path: 'agregar', component: FormularioComponent},
    {path: ':id', component: FormularioComponent}
  ]},
  {path: 'login', component: LoginComponent},
  {path: '**', component: ErrorComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes
    )
  ],
  exports: [RouterModule]

})
export class AppRoutingModule { }
