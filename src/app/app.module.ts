import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// import forms
import {FormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
// service

import { AppComponent } from './app.component';
import { PersonaComponent } from './personas/persona/persona.component';
import { FormularioComponent } from './personas/formulario/formulario.component';
import { EstePrsonasService } from './Personas.service';
import { LoggingService } from './LogginService.service';
import { PersonasComponent } from './personas/personas.component';
import { ErrorComponent } from './error/error.component';
import { DataService } from './data.service';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { LoginService } from './login.service';
import { LoginserviceGuard } from './loginservice.guard';


@NgModule({
  declarations: [
    AppComponent,
    PersonaComponent,
    FormularioComponent,
    PersonasComponent,
    ErrorComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [EstePrsonasService, LoggingService, DataService, LoginService, LoginserviceGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
